## 2.1.0
- DevEco Studio: NEXT Beta1-5.0.3.806,SDK:API12 Release(5.0.0.66)
## 2.1.0-rc.0
- 适配ComponentV2装饰器
- 修改STICKY_LAYOUT布局结合LINEAR_LAYOUT布局滑动固定

## 2.0.0
- 适配DevEco Studio 版本：4.1 Canary(4.1.3.317)，OpenHarmony SDK:API11 (4.1.0.36)
- ArkTS语法适配

## v1.0.5
- 解决了GridLayoutHelper布局colsSpan合并单元格属性引发的高度问题

- 优化了BannerLayoutHelper和SingleLayoutHelper容器高度自适应的实现方式

- 解决了一拖n布局的高度自适应和行列权重的问题

- 解决了左右内外边距传百分比的使布局宽度出现偏差的问题

- 解决了数据源使用@State装饰之后布局不显示的问题，整合了sample页面

- 优化了所有LayoutHelper的代码结构

- 调整了FloatLayoutHelper的实现方式

- 新增了FixAreaAttributes中alignType、defaultLocation属性

- 删除了LinearLayoutHelper、DefaultLayoutHelper的isDifferentHeight属性

- 更改了LinearLayoutHelper、DefaultLayoutHelper的layoutListSpace属性名为dividerHeight

- 新增了ColumnLayoutHelper的colWeights属性

- 新增了FixLayoutHelper、ScrollFixLayoutHelper、StickyLayoutHelper、FloatLayoutHelper的range属性

- 新增了LinearLayoutHelper、DefaultLayoutHelper、BannerLayoutHelper、ColumnLayoutHelper、SingleLayoutHelper的range属性

- 新增了GridLayoutHelper、RangeGridLayoutHelper、StaggeredGridLayoutHelper、OnePlusNLayoutHelper、OnePlusNGridLayoutHelperEx的range属性

- 调整了StaggeredGridLayoutHelper瀑布流布局的构建逻辑

- 调整了GridLayoutHelper、RangeGridLayoutHelper布局的高度自适应问题

- 新增了OnePlusNLayoutHelper、OnePlusNGridLayoutHelperEx的rowWeight、colWeights属性

- 更新了Sample页面

- 取消了Grid和RangeGrid宫格vLayoutId属性设置，使用onAreaChange获取组件宽高

- 解决了改变数据之后grid高度变化异常的问题

- 优化代码，更新README

- 新增了GridLayoutHelper的vLayoutId属性

- 通过组件标识api调整了单行纵横比，单行高度，总高度的计算，取消了oneRowHeight属性

- 优化了Grid宫格高度失效的问题，取消了isAlone属性设置，滑动冲突问题使用宫格高度进行处理

- 调整VLayoutAttributes中Banner属性的拆分

- 新增Grid布局嵌套Banner布局的Sample页面

## v1.0.4

- 优化合并单元格的实现逻辑，autoExpand宽度自适应属性与合并单元格存在互斥关系

- 调整Grid宫格布局的容器高度、单行高度以及单行纵横比的构建

- 变更VLayoutAttributes的属性名与源库一致

- 优化了属性值的判断处理

- 解决了单行纵横比失效以及Grid无法滑动的问题

- 优化了Grid宫格布局的属性，新增isAlone属性，目的是为了在根布局为List时使用grid时滑动没有冲突，又能在根布局为Column中单独使用

- 新增宫格布局滚动Sample页面

- 新增吸顶布局的stickyStart属性

- 新增固定布局的sketchMeasure属性

- 调整RangeGridLayoutHelper的边距属性设置、优化列数和占比的构建

## v1.0.3

- 新增StaggeredGrid宫格瀑布流Sample页面

- 新增lanes瀑布流列数属性和gap瀑布流子项间距属性

- 变更Grid宫格布局的颜色属性为Sample页面自定义

- 新增Grid宫格布局的spanCount列数属性，优化列数和占比的构建

- 新增Grid宫格布局的autoExpand属性，支持Grid宫格布局内的条目自适应：当最后一行条目低于列数时适配

- 优化Grid宫格布局的行高以及总高的获取

- 优化Grid宫格布局的列数和占比的属性设置

- 新增一拖N布局的hasHeader和hasFooter属性

- 完善zIndex当布局重叠时的位置定位

- 新增Grid宫格布局的单行纵横比和单元格纵横比属性

## v1.0.2

- 优化组件的数据类型，新增动态更新布局样式的sample页面

- 新增嵌套组件的sample页面、宫格百分占比sample页面

- 优化组件属性设置，使结构清晰明了

## v1.0.1

- 解决构建出的页面无法进行事件监听、交互以及数据修改的问题

## v1.0.0

- 已实现功能
  - BANNER_LAYOUT滑动容器布局组件
  
  - COLUMN_LAYOUT横向容器布局组件
  
  - DEFAULT_LAYOUT默认容器布局组件
  
  - FIX_LAYOUT固定容器布局组件
  
  - FLOAT_LAYOUT浮动容器布局组件
  
  - GRID_LAYOUT复杂网格容器布局组件
  
  - LINEAR_LAYOUT列表容器布局组件
  
  - ONEN_LAYOUT一加多容器布局组件
  
  - ONEN_EX_LAYOUT一加多拓展容器布局组件
  
  - RANGEGRID_LAYOUT区域网格容器布局组件
  
  - SCROLL_FIX_LAYOUT滚动固定容器布局组件
  
  - SINGLE_LAYOUT单项容器布局组件
  
  - STAGGEREDGRID_LAYOUT交错网格容器布局组件
  
  - STICKY_LAYOUT吸顶容器布局组件
  
  - 跳转item项位置
  
- 遗留问题
  
  - TotalOffset总偏移量暂时无法获取
