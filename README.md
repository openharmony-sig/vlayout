# VLayout

## Introduction

> VLayout is applicable when the list, grid, and other layouts are involved in the same view. You can use the preset container layout components or customize the container layout components as required based on the existing components.

## Effect

1. BANNER_LAYOUT: Scrolling container layout component.

   <img src="./gifs/BANNER_LAYOUT.gif"/>

2. COLUMN_LAYOUT: Column container layout component.

   <img src="./gifs/COLUMN_LAYOUT.gif"/>

3. DEFAULT_LAYOUT: Default container layout component.

   <img src="./gifs/DEFAULT_LAYOUT.gif"/>

4. FIX_LAYOUT: Fixed container layout component.

   <img src="./gifs/FIX_LAYOUT.gif"/>

5. FLOAT_LAYOUT: Floating container layout component.

   <img src="./gifs/FLOAT_LAYOUT.gif"/>

6. GRID_LAYOUT: Complex grid container layout component.

   <img src="./gifs/GRID_LAYOUT.gif"/>

7. LINEAR_LAYOUT: List container layout component.

   <img src="./gifs/LINEAR_LAYOUT.gif"/>

8. ONEN_LAYOUT: One-to-N container layout component.

   <img src="./gifs/ONEN_LAYOUT.gif"/>

9. ONEN_EX_LAYOUT: One-to-N extension container layout component.

   <img src="./gifs/ONEN_EX_LAYOUT.gif"/>

10. RANGEGRID_LAYOUT: Region grid container layout component.

   <img src="./gifs/RANGEGRID_LAYOUT.gif"/>

11. SCROLL_FIX_LAYOUT: Scrolling-fix container layout component.

    <img src="./gifs/SCROLL_FIX_LAYOUT.gif"/>

12. SINGLE_LAYOUT: Banner container layout component.

    <img src="./gifs/SINGLE_LAYOUT.gif"/>

13. STAGGEREDGRID_LAYOUT: Interleaved grid container layout component.

    <img src="./gifs/STAGGEREDGRID_LAYOUT.gif"/>

14. STICKY_LAYOUT: Container layout component pinned to the top.

    <img src="./gifs/STICKY_LAYOUT.gif"/>

15. Redirecting to the position of an item.

    <img src="./gifs/JUMP.gif"/>

## How to Install

1. For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

2. Run the installation command:

   ```
   ohpm install @ohos/vlayout
   ```

## How to Use

### Geting Started

1. The use of VLayout implemented using OpenHarmony ArkTS differs slightly from the source library. This document describes how to use such VLayout custom container components.

2. To use a VLayout custom container component, you need to specify the layout content to be displayed (vLayoutContent), data source (vLayoutData), and container attribute (vLayoutAttribute).

3. To use the 14 types of custom container components provided by VLayout: 1. Import the required container components. 2. Create the corresponding container component objects and provide the layout content to be displayed.

4. Limitations:

 1. It is recommended that VLayout be previewed by using the Previewer with a screen width of 720, screen height of 1280, and DPI of 240.

 2. If the native **Grid** component of ArkTS does not have the **rowsTemplate** attribute, **Grid** can perform internal scrolling. However, if **Grid** is nested in a scrollable **List** component, internal scrolling of **Grid** becomes invalid when **List** starts scrolling. That is, the scrolling event of **Grid** becomes invalid when the **List-ListItem** component starts a scrolling event. To enable internal scrolling of **Grid**, the grid height must be set and **ListItem** must be unscrollable. To disable it, do not set the grid height.

 3. **GRID_LAYOUT** provides the function of merging cells. To merge cells, you need to add **key** of **colsSpan** to the data source.

 4. When **FLOAT_LAYOUT**, **FIX_LAYOUT**, **SCROLL_FIX_LAYOUT**, and **STICKY_LAYOUT** are used, the parent container must use the **Stack** container for loading.

 5. In the following example, if the parent container is **Column**, the root layout can be an existing basic container such as **Column** or **List**.

 6. It is recommended that the parent container is used with **Stack**, **List**, and **ListItem** or with** Scroll** and **Column**, among which only the former combination can be used for a fixed layout.

 7. Layout containers whose width and height can be explicitly provided include **BANNER_LAYOUT**, **SINGLE_LAYOUT**, **ONEN_LAYOUT**, **ONEN_EX_LAYOUT**, **FIX_LAYOUT**, **SCROLL_FIX_LAYOUT**, **STICKY_LAYOUT**, and **FLOAT_LAYOUT** (valid only when the **layoutHeight** attribute is provided and the left and right margins are specified).

 8. Layout containers whose height is calculated by the component provided by **Builder** include **GRID_LAYOUT**, **RANGEGRID_LAYOUT**, **STAGGEREDGRID_LAYOUT**, **LINEAR_LAYOUT**, **DEFAULT_LAYOUT**, and **COLUMN_LAYOUT**. The height of some containers, such as the last two containers, cannot be explicitly provided. When the layout height is less than the total height of the components provided by **Builder**, internal scrolling is supported.

 9. If a user provides both the **aspectRatio** attribute and the height of the component in **Builder**, the layout is abnormal. Troubleshoot by setting **height** to **100%** or to the height value transferred from the layout.

 10. For details about how to use the demo, see the implementation of the sample page.

### Available APIs

1. When building a custom container component, you must specify the following attributes:

| Name                                           | Mandatory| Description (Different Layout Components with Different Attributes)|
| ----------------------------------------------- | -------- | ---------------------------------- |
| vLayoutContent                                  | Yes      | Layout. Use @Builder to build a custom page.  |
| vLayoutData: layoutDataType[],number[],string[] | Yes      | Data source.                            |
| vLayoutAttribute: Attributes                    | Yes      | Type.                          |

2. The VLayout attributes are set in declaration mode to facilitate the management of the attributes of each container layout class. The following is the **vLayoutAttribute** attribute class, which is optional.

```
export class GridAttributes {
  /*
   * GridLayoutHelper
   * RangeGridLayoutHelper
   */
  range?: Array<number> // Number of items that can be displayed. The default value is [0, *data source length*].
  spanCount?: number // Number of columns. The default value is 1.
  weights?: Array<number> // Column proportion. The value is the ratio of the number of elements in the set to the number of elements in the array. The default value is 100.
  autoExpand?: boolean // Width adaptive of the remainder subitems. This parameter is valid when weights is not defined and is mutually exclusive with colsSpan. The default value is true.
  aspectRatio?: number // Single-line aspect ratio of GridItem. The default value is 0.
  layoutHeight?: Length // Height of the Grid container.
  bgColor?: ResourceColor // Background color of the Grid container. The default value is transparent.
  zIndex?: number // Z-order. The default value is 0.
  gap?: number // Row spacing and column spacing. The default value is 0.
  vGap?: number // Row spacing. The default value is 0.
  hGap?: number // Column spacing. The default value is 0.
  padding?: Length[] // Padding. The default value is []. The priority is higher than that of a single-direction padding.
  topPadding?: Length // Padding-top. The default value is 0.
  rightPadding?: Length // Padding-right. The default value is 0.
  bottomPadding?: Length // Padding-bottom. The default value is 0.
  leftPadding?: Length // Padding-left. The default value is 0.
  margin?: Length[] // Margin. The default value is []. The priority is higher than that of a single-direction margin.
  topMargin?: Length // Margin-top. The default value is 0.
  rightMargin?: Length // Margin-right. The default value is 0.
  bottomMargin?: Length // Margin-bottom. The default value is 0.
  leftMargin?: Length // Margin-left. The default value is 0.
}

export class StaggeredGridAttributes {
  /*
   * StaggeredGridLayoutHelper
   */
  range?: Array<number> // Number of items that can be displayed. The default value is [0, *data source length*].
  lanes?: number // Number of columns. The default value is 1.
  bgColor?: ResourceColor // Background color of a container. The default value is transparent.
  zIndex?: number // Z-order. The default value is 0.
  gap?: number // Row spacing and column spacing. The default value is 0.
  vGap?: number // Row spacing. The default value is 0.
  hGap?: number // Column spacing. The default value is 0.
  padding?: Length[] // Padding. The default value is []. The priority is higher than that of a single-direction padding.
  topPadding?: Length // Padding-top. The default value is 0.
  rightPadding?: Length // Padding-right. The default value is 0.
  bottomPadding?: Length // Padding-bottom. The default value is 0.
  leftPadding?: Length // Padding-left. The default value is 0.
  margin?: Length[] // Margin. The default value is []. The priority is higher than that of a single-direction margin.
  topMargin?: Length // Margin-top. The default value is 0.
  rightMargin?: Length // Margin-right. The default value is 0.
  bottomMargin?: Length // Margin-bottom. The default value is 0.
  leftMargin?: Length // Margin-left. The default value is 0.
}

export class AbstractFullFillAttributes {
  /*
   * SingleLayoutHelper
   * ColumnLayoutHelper
   * OnePlusNLayoutHelper
   * OnePlusNLayoutHelperEx
   */
  range?: Array<number> // Number of items that can be displayed. The default value is [0, *data source length*].
  rowWeights?: Array<number> // Row weight. The default value is []. This parameter is supported only by OnePlusNLayoutHelper and OnePlusNLayoutHelperEx.
  colWeights?: Array<number> // Column weight. The default value is [].
  hasHeader?: boolean // The first subitem is displayed in one row and one column. The default value is false. Only OnePlusNLayoutHelper supports this function.
  hasFooter?: boolean // The last subitem is displayed in one row and one column. The default value is false. Only OnePlusNLayoutHelper supports this function.
  layoutWidth?: Length // Container width.
  layoutHeight?: Length // Container height.
  aspectRatio?: number // Aspect ratio of a container.
  bgColor?: ResourceColor // Background color of a container. The default value is transparent.
  zIndex?: number // Z-order. The default value is 0.
  padding?: Length[] // Padding. The default value is []. The priority is higher than that of a single-direction padding.
  topPadding?: Length // Padding-top. The default value is 0.
  rightPadding?: Length // Padding-right. The default value is 0.
  bottomPadding?: Length // Padding-bottom. The default value is 0.
  leftPadding?: Length // Padding-left. The default value is 0.
  margin?: Length[] // Margin. The default value is []. The priority is higher than that of a single-direction margin.
  topMargin?: Length // Margin-top. The default value is 0.
  rightMargin?: Length // Margin-right. The default value is 0.
  bottomMargin?: Length // Margin-bottom. The default value is 0.
  leftMargin?: Length // Margin-left. The default value is 0.
}

export class BannerAttributes {
  /*
   * BannerLayoutHelper
   */
  range?: Array<number> // Number of items that can be displayed. The default value is [0, *data source length*].
  layoutWidth?: Length // Container width.
  layoutHeight?: Length // Container height.
  aspectRatio?: number // Aspect ratio of a container.
  bgColor?: ResourceColor // Background color of a container. The default value is transparent.
  layoutIndex?: number // Index of the child component currently displayed in the container. The default value is 0.
  layoutAutoPlay?: boolean // Whether to enable automatic playback for child components. If this attribute is true, the navigation dots indicator does not take effect. The default value is false.
  layoutInterval?: number // Interval for automatic playback, in ms. The default value is 1000.
  layoutIndicator?: boolean // Whether to enable the navigation dots indicator. The default value is false.
  layoutLoop?: boolean // Whether to enable loop playback. The default value is false.
  layoutDuration?: number // Duration of the animation for switching child components, in milliseconds. The default value is 400.
  layoutVertical?: boolean // Whether vertical scrolling is used. The default value is false.
  layoutItemSpace?: number | string // Spacing between child components. The default value is 0.
  layoutEffectMode?: EdgeEffect // Scrolling effect. The default value is None.
  zIndex?: number // Z-order. The default value is 0.
  padding?: Length[] // Padding. The default value is []. The priority is higher than that of a single-direction padding.
  topPadding?: Length // Padding-top. The default value is 0.
  rightPadding?: Length // Padding-right. The default value is 0.
  bottomPadding?: Length // Padding-bottom. The default value is 0.
  leftPadding?: Length // Padding-left. The default value is 0.
  margin?: Length[] // Margin. The default value is []. The priority is higher than that of a single-direction margin.
  topMargin?: Length // Margin-top. The default value is 0.
  rightMargin?: Length // Margin-right. The default value is 0.
  bottomMargin?: Length // Margin-bottom. The default value is 0.
  leftMargin?: Length // Margin-left. The default value is 0.
}

export class LinearAttributes {
  /*
   * LinearLayoutHelper
   * DefaultLayoutHelper
   */
  range?: Array<number> // Number of items that can be displayed. The default value is [0, *data source length*].
  dividerHeight?: number | string // Vertical spacing between list items. The default value is 0.
  layoutHeight?: Length // Container height.
  aspectRatio?: number // Single-line aspect ratio of ListItem. The default value is 0.
  bgColor?: ResourceColor // Background color of a container. The default value is transparent.
  zIndex?: number // Z-order. The default value is 0.
  padding?: Length[] // Padding. The default value is []. The priority is higher than that of a single-direction padding.
  topPadding?: Length // Padding-top. The default value is 0.
  rightPadding?: Length // Padding-right. The default value is 0.
  bottomPadding?: Length // Padding-bottom. The default value is 0.
  leftPadding?: Length // Padding-left. The default value is 0.
  margin?: Length[] // Margin. The default value is []. The priority is higher than that of a single-direction margin.
  topMargin?: Length // Margin-top. The default value is 0.
  rightMargin?: Length // Margin-right. The default value is 0.
  bottomMargin?: Length // Margin-bottom. The default value is 0.
  leftMargin?: Length // Margin-left. The default value is 0.
}

export enum AlignType {
  TOP_LEFT = 0,
  TOP_RIGHT = 1,
  BOTTOM_LEFT = 2,
  BOTTOM_RIGHT = 3
}

export class FixAreaAttributes {
  /*
   * FixLayoutHelper
   * ScrollFixLayoutHelper
   * StickyLayoutHelper
   * FloatLayoutHelper
   */
  range?: Array<number> // Number of items that can be displayed. The default value is [0, *data source length*].
  layoutWidth?: Length // Container width.
  layoutHeight?: Length // Container height, which is valid when the aspect ratio of the container is 0.
  aspectRatio?: number // Aspect ratio of a container.
  bgColor?: ResourceColor // Background color of a container. The default value is transparent.
  xOffset?: Length // Horizontal offset. The default value is 0. It is supported by all components except StickyLayoutHelper.
  yOffset?: Length // Vertical offset. The default value is 0. It is supported by all components except StickyLayoutHelper.
  alignType?: AlignType // Fixed position, whose priority is higher than that of xOffset?: Length or yOffset?: Length. It is supported by all components except StickyLayoutHelper. In FloatLayoutHelper, the value is accumulated with that of defaultLocation.
  sketchMeasure?: boolean // Width occupying the full screen. Only FixLayoutHelper and ScrollFixLayoutHelper support this function.
  stickyStart?: boolean // The value can be true or false, indicates pinning to the top or pinning to the bottom, respectively. It is supported only by StickyLayoutHelper.
  defaultLocation?: number[] // Default position, whose priority is higher than that of xOffset?: Length or yOffset?: Length. It is supported only by FloatLayoutHelper. The value is accumulated with that of alignType.
  zIndex?: number // Z-order. The default value is 0.
  padding?: Length[] // Padding. The default value is []. The priority is higher than that of a single-direction padding.
  topPadding?: Length // Padding-top. The default value is 0.
  rightPadding?: Length // Padding-right. The default value is 0.
  bottomPadding?: Length // Padding-bottom. The default value is 0.
  leftPadding?: Length // Padding-left. The default value is 0.
  margin?: Length[] // Margin. The default value is []. The priority is higher than that of a single-direction margin.
  topMargin?: Length // Margin-top. The default value is 0.
  rightMargin?: Length // Margin-right. The default value is 0.
  bottomMargin?: Length // Margin-bottom. The default value is 0.
  leftMargin?: Length // Margin-left. The default value is 0.
}

export class dataType {
  layoutData: number[] | layoutDataType[] | string[] = []
  rowsTemplate?: string[] = []
}

export class layoutDataType {
  layoutText: number | string = 0
  colsSpan?: number = 0
  layoutColor?: string = '#FF0000'
  layoutWeight?: number = 1
  textSize?: number = 25
  textColor?: string = '#999999'
  bgColor?: string = '#CFCFCF'
  top?: number = 0
  left?: number = 0
  tag?: string = ''
}
```

### How to Use GRID_LAYOUT

1. Import the **GRID_LAYOUT** container.

 ```
import { GRID_LAYOUT } from '@ohos/vlayout'
 ```

2. Create a **GRID_LAYOUT** object and provide the layout content to be displayed.

 ```
@Builder gridLayoutContent(item: layoutDataType, position: number, gridItemHeight: number) {
  Text(`${item.layoutText}`)
    .width('100%')
    .height(200)
    .backgroundColor(0x33EEEEEE)
    .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
    .fontColor(0x999999)
    .fontSize(50)
    .fontWeight(FontWeight.Bold)
    .textAlign(TextAlign.Center)
    .onClick(() => {
      console.info('position = ' + position)
    })
}

build() {
  Column() {
    GRID_LAYOUT({
      vLayoutContent: (item: layoutDataType, position: number, gridItemHeight: number) => {
        this.gridLayoutContent(item, position, gridItemHeight)
      },
      vLayoutData: [
        { layoutText: 1 }, { layoutText: 2 }, { layoutText: 3 }, { layoutText: 4 },
        { layoutText: 5 }, { layoutText: 6 }, { layoutText: 7 }, { layoutText: 8 },
      ],
      vLayoutAttribute: {
        range: [0, 7],
        spanCount: 3,
        weights: [25, 25],
        layoutHeight: 300,
        bgColor: Color.Pink,
        gap: 10,
        padding: [10, 10, 10, 10],
        margin: [10, 10, 10, 10],
      }
    })
  }
}
 ```

### How to Use RANGEGRID_LAYOUT

1. Import the **RANGEGRID_LAYOUT** container.

```
import { RANGEGRID_LAYOUT } from '@ohos/vlayout'
```

2. Create a **RANGEGRID_LAYOUT** object and provide the layout content to be displayed.

```
@Builder rangeGridLayoutContent(item: layoutDataType, position: number, gridItemHeight: number) {
  Column() {
    Text(`${item.layoutText}`)
      .width('100%')
      .height('100%')
      .backgroundColor(0x22EEEEEE)
      .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
      .fontColor('#999999')
      .fontSize(25)
      .fontWeight(FontWeight.Bold)
      .maxLines(1)
      .textAlign(TextAlign.Center)
      .textOverflow({ overflow: TextOverflow.Ellipsis })
      .onClick(() => {
        console.info('position = ' + position)
      })
  }
  .backgroundColor(item.bgColor)
  .padding(5)
  .margin({
    top: position == 4 || position == 5 || position == 8 || position == 9 ? 5 : 0,
    right: position == 5 || position == 7 || position == 9 || position == 11 ? 10 : 0,
    bottom: position == 6 || position == 7 || position == 10 || position == 11 ? 5 : 0,
    left: position == 4 || position == 6 || position == 8 || position == 10 ? 10 : 0
  })
}

build() {
  Column() {
    RANGEGRID_LAYOUT({
      vLayoutContent: (item: layoutDataType, position: number, gridItemHeight: number) => {
        this.rangeGridLayoutContent(item, position, gridItemHeight)
      },
      vLayoutData: [
        { layoutText: 1 },
        { layoutText: 2 },
        { layoutText: 3 },
        { layoutText: 4 },
        { layoutText: 5, colsSpan: 2, bgColor: '#FF0000' },
        { layoutText: 6, colsSpan: 2, bgColor: '#FF0000' },
        { layoutText: 7, colsSpan: 2, bgColor: '#FF0000' },
        { layoutText: 8, colsSpan: 2, bgColor: '#FF0000' },
        { layoutText: 9, colsSpan: 2, bgColor: '#FFFF00' },
        { layoutText: 10, colsSpan: 2, bgColor: '#FFFF00' },
        { layoutText: 11, colsSpan: 2, bgColor: '#FFFF00' },
        { layoutText: 12, colsSpan: 2, bgColor: '#FFFF00' },
        { layoutText: 13 },
        { layoutText: 14 },
        { layoutText: 15 },
        { layoutText: 16 },
      ],
      vLayoutAttribute: {
        spanCount: 4,
        weights: [20, 26.6, 26.6, 26.6],
        aspectRatio: 4,
        bgColor: '#00FF00',
        padding: [10, 10, 10, 10],
        margin: [10, 10, 10, 10]
      }
    })
  }
}
```

### How to Use STAGGEREDGRID_LAYOUT

1. Import the **STAGGEREDGRID_LAYOUT** container.

```
import { STAGGEREDGRID_LAYOUT } from '@ohos/vlayout'
```

2. Create a **STAGGEREDGRID_LAYOUT** object and provide the layout content to be displayed.

```
@Builder staggeredGridLayoutContent(item: layoutDataType, position: number) {
  if (position % 2 == 0) {
    Text(`${item.layoutText}`)
      .width('100%')
      .height(220)
      .backgroundColor('#33EEEEEE')
      .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
      .fontColor('#999999')
      .fontSize(25)
      .fontWeight(FontWeight.Bold)
      .textAlign(TextAlign.Center)
      .textOverflow({ overflow: TextOverflow.Ellipsis })
      .onClick(() => {
        console.info('position = ' + position + ', item = ' + JSON.stringify(item))
      })
  } else {
    Text(`${item.layoutText}`)
      .width('100%')
      .height(150)
      .backgroundColor('#33EEEEEE')
      .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
      .fontColor('#999999')
      .fontSize(25)
      .fontWeight(FontWeight.Bold)
      .textAlign(TextAlign.Center)
      .textOverflow({ overflow: TextOverflow.Ellipsis })
      .onClick(() => {
        console.info('position = ' + position + ', item = ' + JSON.stringify(item))
      })
  }
}

build() {
  Column() {
    STAGGEREDGRID_LAYOUT({
      vLayoutContent: (item: layoutDataType, position: number) => {
        this.staggeredGridLayoutContent(item, position)
      },
      vLayoutData: [
        { layoutText: 1 },
        { layoutText: 2 },
        { layoutText: 3 },
        { layoutText: 4 },
        { layoutText: 5 },
        { layoutText: 6 },
        { layoutText: 7 },
        { layoutText: 8 },
        { layoutText: 9 },
        { layoutText: 10 },
      ],
      vLayoutAttribute: {
        range: [0, 8],
        lanes: 3,
        gap: 10,
        bgColor: Color.Pink,
        padding: [10, 10, 10, 10],
        margin: [10, 10, 10, 10],
      },
      vLayoutId: 'STAGGERED'
    })
  }
}
```

### How to Use COLUMN_LAYOUT

1. Import the **COLUMN_LAYOUT** container.

```
import { COLUMN_LAYOUT } from '@ohos/vlayout'
```

2. Create a **COLUMN_LAYOUT** object and provide the layout content to be displayed.

```
@Builder columnLayoutContent(item: layoutDataType, position: number | undefined, layoutHeight: number | undefined) {
  Text(`${item.layoutText}`)
    .width('100%')
    .height(100)
    .backgroundColor(0x22EEEEEE)
    .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
    .fontSize(25)
    .fontColor('#999999')
    .fontWeight(FontWeight.Bold)
    .textAlign(TextAlign.Center)
    .textOverflow({ overflow: TextOverflow.Ellipsis })
    .onClick(() => {
      console.info('position = ' + position)
    })
}

build() {
  Column() {
    COLUMN_LAYOUT({
      vLayoutContent: (item: layoutDataType, position: number | undefined, layoutHeight: number | undefined) => {
        this.columnLayoutContent(item, position, layoutHeight)
      },
      vLayoutData: [
        { layoutText: 1 },
        { layoutText: 2 },
        { layoutText: 3 },
        { layoutText: 4 },
        { layoutText: 5 },
      ],
      vLayoutAttribute: {
        bgColor: Color.Pink,
        colWeights: [30, 20, 20],
        padding: [10, 10, 10, 10],
        margin: [10, 10, 10, 10],
      }
    })
  }
}
```

### How to Use SINGLE_LAYOUT

1. Import the **SINGLE_LAYOUT** container.

```
import { SINGLE_LAYOUT } from '@ohos/vlayout'
```

2. Create a **SINGLE_LAYOUT** object and provide the layout content to be displayed.

```
@Builder singleLayoutContent(item: layoutDataType, position: number) {
  Text(`${item}`)
    .width('100%')
    .height('100%')
    .backgroundColor(0x22EEEEEE)
    .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
    .fontSize(25)
    .fontColor('#999999')
    .fontWeight(FontWeight.Bold)
    .maxLines(1)
    .textAlign(TextAlign.Center)
    .textOverflow({ overflow: TextOverflow.Ellipsis })
    .onClick(() => {
      console.info('position = ' + position)
    })
}

build() {
  Column() {
    SINGLE_LAYOUT({
      vLayoutContent: (item: layoutDataType, position: number | undefined) => {
        this.singleLayoutContent(item, position)
      },
      vLayoutData: ['SINGLE'],
      vLayoutAttribute: {
        layoutHeight: 70,
        bgColor: Color.Blue,
        topMargin: 5,
        bottomMargin: 90,
      }
    })
  }
}
```

### How to Use ONEN_LAYOUT

1. Import the **ONEN_LAYOUT** container.

```
import { ONEN_LAYOUT }from '@ohos/vlayout'
```

2. Create a **ONEN_LAYOUT** object and provide the layout content to be displayed.

```
@Builder onenLayoutContent(item: layoutDataType) {
  Text(`${item.layoutText}`)
    .width('100%')
    .height('100%')
    .backgroundColor(0x22EEEEEE)
    .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
    .fontSize(25)
    .fontColor('#999999')
    .fontWeight(FontWeight.Bold)
    .textAlign(TextAlign.Center)
    .textOverflow({ overflow: TextOverflow.Ellipsis })
    .onClick(() => {
  	  console.info('item = ' + JSON.stringify(item))
	})
}

build() {
  Column() {
    ONEN_LAYOUT({
      vLayoutContent: (item: layoutDataType) => {
        this.onenLayoutContent(item)
      },
      vLayoutData: [1, 2, 3, 4, 5],
      vLayoutAttribute: {
        rowWeights: [10, 20],
        colWeights: [100, 20, 30, 40, 10],
        hasHeader: true,
        hasFooter: false,
        layoutHeight: 200,
        bgColor: Color.Pink,
        padding: [5, 5, 5, 5],
        margin: [5, 15, 5, 15],
      }
    })
  }
}
```

### How to Use ONEN_EX_LAYOUT

1. Import the **ONEN_EX_LAYOUT** container.

```
import { ONEN_EX_LAYOUT }from '@ohos/vlayout'
```

2. Create a **ONEN_EX_LAYOUT** object and provide the layout content to be displayed.

```
@Builder onenExLayoutContent(item: layoutDataType) {
  Text(`${item.layoutText}`)
    .width('100%')
    .height('100%')
    .backgroundColor('#33EEEEEE')
    .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
    .fontColor('#999999')
    .fontSize(25)
    .fontWeight(FontWeight.Bold)
    .textAlign(TextAlign.Center)
    .textOverflow({ overflow: TextOverflow.Ellipsis })
    .onClick(() => {
      console.info('item = ' + JSON.stringify(item))
    })
}

build() {
  Column() {
    ONEN_EX_LAYOUT({
      vLayoutContent: (item: layoutDataType) => {
        this.onenExLayoutContent(item)
      },
      vLayoutData: [1, 2, 3, 4, 5, 6],
      vLayoutAttribute: {
        rowWeights: [10, 20],
        colWeights: [40, 45, 15, 10, 30, 30],
        layoutHeight: 200,
        bgColor: Color.Pink,
        padding: [5, 5, 5, 5],
        margin: [5, 15, 5, 15],
      }
    })
  }
}
```

### How to Use BANNER_LAYOUT

1. Import the **BANNER_LAYOUT** container.

```
import { BANNER_LAYOUT }from '@ohos/vlayout'
```

2. Create a **BANNER_LAYOUT** object and provide the layout content to be displayed.

```
@Builder bannerLayoutContent(item: layoutDataType, position: number) {
  Text('Banner:' + item)
    .width('100%')
    .height('100%')
    .backgroundColor(0x22EEEEEE)
    .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
    .fontSize(20)
    .fontColor('#999999')
    .fontWeight(FontWeight.Bold)
    .maxLines(1)
    .textAlign(TextAlign.Center)
    .textOverflow({ overflow: TextOverflow.Ellipsis })
    .onClick(() => {
      console.info('position = ' + position)
    })
}

build() {
  Column() {
    BANNER_LAYOUT({
      vLayoutContent: (item: layoutDataType, position: number | undefined) => {
        this.bannerLayoutContent(item, position)
      },
      vLayoutData: [0, 1, 2, 3, 4, 5],
      vLayoutAttribute: {
        aspectRatio: 4,
        bgColor: Color.Pink,
        padding: [10, 10, 10, 10],
        margin: [10, 0, 10, 0],
      },
    })
  }
}
```

### How to Use LINEAR_LAYOUT

1. Import the **LINEAR_LAYOUT** container.

```
import { LINEAR_LAYOUT }from '@ohos/vlayout'
```

2. Create a **LINEAR_LAYOUT** object and provide the layout content to be displayed.

```
@Builder linearLayoutContent(item: layoutDataType, position: number) {
  Text(`${item}`)
    .width('100%')
    .height('100%')
    .backgroundColor(0x22EEEEEE)
    .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
    .fontSize(25)
    .fontColor('#999999')
    .fontWeight(FontWeight.Bold)
    .maxLines(1)
    .textAlign(TextAlign.Center)
    .textOverflow({ overflow: TextOverflow.Ellipsis })
    .onClick(() => {
      console.info('position = ' + position)
    })
}

build() {
  Column() {
    LINEAR_LAYOUT({
      vLayoutContent: (item: layoutDataType, position: number | undefined) => {
        this.linearLayoutContent(item, position)
      },
      vLayoutData: [0, 1, 2, 3, 4, 5, 6],
      vLayoutAttribute: {
        dividerHeight: 10,
        aspectRatio: 4,
        layoutHeight: 500,
        bgColor: Color.Pink,
        padding: [10, 10, 10, 10],
        margin: [10, 10, 10, 10],
      },
    })
  }
}
```

### How to Use DEFAULT_LAYOUT

1. Import the **DEFAULT_LAYOUT** container.

```
import { DEFAULT_LAYOUT }from '@ohos/vlayout'
```

2. Create a **DEFAULT_LAYOUT** object and provide the layout content to be displayed.

```
@Builder defaultLayoutContent(item: layoutDataType, position: number | undefined) {
  Text(`${item}`)
    .width('100%')
    .height('100%')
    .backgroundColor(position % 2 == 0 ? '#aa00ff00' : '#ccff00ff')
    .border({ width: 0, color: '#000000', radius: 0, style: BorderStyle.Solid })
    .fontColor(0x000000)
    .fontSize(18)
    .fontWeight(FontWeight.Normal)
    .textAlign(TextAlign.Start)
    .align(Alignment.TopStart)
    .textOverflow({ overflow: TextOverflow.Ellipsis })
    .onClick(() => {
      console.info('position = ' + position)
    })
}

build() {
  Column() {
    DEFAULT_LAYOUT({
      vLayoutContent: (item: layoutDataType, position: number | undefined) => {
        this.defaultLayoutContent(item, position)
      },
      vLayoutData: [0, 1, 2, 3, 4, 5],
      vLayoutAttribute: {
        dividerHeight: 5,
        aspectRatio: 4,
        margin: [5, 5, 5, 5],
      }
    })
  }
}
```

### How to Use FLOAT_LAYOUT

1. Import the **FLOAT_LAYOUT** container.

```
import { FLOAT_LAYOUT } from '@ohos/vlayout'
```

2. Create a **FLOAT_LAYOUT** object and provide the layout content to be displayed. Because the floating component needs to be displayed in a stacked manner in the parent container, you are advised to use **Stack** as the root layout.

```
enum AlignType {
  TOP_LEFT = 0,
  TOP_RIGHT = 1,
  BOTTOM_LEFT = 2,
  BOTTOM_RIGHT = 3
}

@Builder floatLayoutContent(item: layoutDataType, position: number) {
  Text(`${item}`)
    .width('100%')
    .height('100%')
    .textAlign(TextAlign.Center)
    .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
}

build() {
  Stack() {
    Text('TEXT')
    
    FLOAT_LAYOUT({
      vLayoutContent: (item: layoutDataType, position: number | undefined) => {
        this.floatLayoutContent(item, position)
      },
      vLayoutData: ['FLOAT'],
      vLayoutAttribute: {
        layoutWidth: 150,
        layoutHeight: 150,
        bgColor: Color.Pink,
        defaultLocation: [-50, -150],
        alignType: AlignType.BOTTOM_RIGHT,
      }
    })
  }
}
```

### How to Use FIX_LAYOUT

1. Import the **FIX_LAYOUT** container.

```
import { FIX_LAYOUT } from '@ohos/vlayout'
```

2. Create a **FIX_LAYOUT** object and provide the layout content to be displayed. Because the fixed component needs to be displayed in a stacked manner in the parent container, you are advised to use **Stack** as the root layout.

```
enum AlignType {
  TOP_LEFT = 0,
  TOP_RIGHT = 1,
  BOTTOM_LEFT = 2,
  BOTTOM_RIGHT = 3
}

@Builder fixLayoutContent(item: layoutDataType, position: number) {
  Text(`${item}`)
    .width('100%')
    .height('100%')
    .backgroundColor(0x22EEEEEE)
    .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
    .fontSize(25)
    .fontColor('#999999')
    .fontWeight(FontWeight.Bold)
    .textAlign(TextAlign.Center)
    .textOverflow({ overflow: TextOverflow.Ellipsis })
    .onClick(() => {
      console.info('position = ' + position)
    })
}

build() {
  Stack() {
    Text('TEXT')
    FIX_LAYOUT({
      vLayoutContent: (item: layoutDataType, position: number | undefined) => {
        this.fixLayoutContent(item, position)
      },
      vLayoutData: ['FIX'],
      vLayoutAttribute: {
        layoutWidth: 100,
        layoutHeight: 100,
        sketchMeasure: false,
        bgColor: Color.Pink,
        alignType: AlignType.TOP_RIGHT,
      }
    })
  }
}
```

### How to Use SCROLL_FIX_LAYOUT

1. Import the **SCROLL_FIX_LAYOUT** container.

```
import { SCROLL_FIX_LAYOUT }from '@ohos/vlayout'
```

2. Create a **SCROLL_FIX_LAYOUT** object and provide the layout content to be displayed. The component needs to be displayed in the parent container in a stacked manner and needs to be displayed after the parent container is scrolled to a certain position. Therefore, you are advised to use **Stack** as the root layout, and implement this effect in the parent container together with **JumpBar**.

```
enum AlignType {
  TOP_LEFT = 0,
  TOP_RIGHT = 1,
  BOTTOM_LEFT = 2,
  BOTTOM_RIGHT = 3
}

@Builder scrollFixLayoutContent(item: layoutDataType, position: number | undefined) {
  Text(`${item}`)
    .width('100%')
    .height('100%')
    .backgroundColor(0x22EEEEEE)
    .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
    .fontSize(25)
    .fontColor(0x999999)
    .fontWeight(FontWeight.Bold)
    .textAlign(TextAlign.Center)
    .textOverflow({ overflow: TextOverflow.Ellipsis })
    .onClick(() => {
      console.info('position = ' + position)
    })
}

build() {
  Stack({ alignContent: Alignment.Bottom }) {
    SCROLL_FIX_LAYOUT({
      vLayoutContent: (item: layoutDataType, position: number | undefined) => {
        this.scrollFixLayoutContent(item, position)
      },
      vLayoutData: [7],
      vLayoutAttribute: {
        layoutWidth: 15,
        layoutHeight: 25,
        bgColor: '#ccff00ff',
        xOffset: '85%',
        yOffset: '5%',
      }
    })
  }
}
```

### How to Use STICKY_LAYOUT

1. Import the **STICKY_LAYOUT** container.

```
import { STICKY_LAYOUT } from '@ohos/vlayout'
```

2. Create a **STICKY_LAYOUT** object and provide the layout content to be displayed. The component needs to be displayed in the parent container in a stacked manner and is pinned to the top or to the bottom after the parent container is scrolled to a certain position. Therefore, you are advised to use **Stack** together with **List-ListItem** as the root layout, and implement this effect in the parent container.

```
@Builder stickyLayoutContent(item: layoutDataType, position: number) {
  Text(`${item}`)
    .width('100%')
    .height('100%')
    .backgroundColor(0x22EEEEEE)
    .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
    .fontSize(25)
    .fontColor('#999999')
    .fontWeight(FontWeight.Bold)
    .textAlign(TextAlign.Center)
    .textOverflow({ overflow: TextOverflow.Ellipsis })
    .onClick(() => {
      console.info('position = ' + position)
    })
}

build() {
  List() {
    ListItem() {
      STICKY_LAYOUT({
        vLayoutContent: (item: layoutDataType, position: number | undefined) => {
          this.stickyLayoutContent(item, position)
        },
        vLayoutData: ['STICKY'],
        vLayoutAttribute: {
          aspectRatio: 4,
          bgColor: '#22EEEEEE',
        }
      })
    }.sticky(Sticky.Normal)
  }.edgeEffect(EdgeEffect.None)
}
```

## Constraints

This project has been verified in the following versions:

- DevEco Studio: 5.0 Canary (5.0.3.502)

- OpenHarmony SDK: API 12 (5.0.0.31)

## Directory Structure

```
|---- vlayout  
|     |---- entry  # Sample code
|     |---- library  # VLayout library
|           |---- index.ets  # External APIs
|           |---- src
|		|---- main
|			|---- ets
|				|---- components
|					|---- common  # Component folder
|						|---- BannerLayoutHelper.ets  # Scrolling container layout component
|						|---- ColumnLayoutHelper.ets  # Horizontal container layout component
|						|---- DefaultLayoutHelper.ets  # Default linear container layout component
|						|---- FixLayoutHelper.ets  # Fixed container layout component
|						|---- FloatLayoutHelper.ets  # Floating container layout component
|						|---- GridLayoutHelper.ets  # Grid container layout component
|						|---- JumpBar.ets  # Redirection function bar
|						|---- LinearLayoutHelper.ets  # List container layout component
|						|---- OnePlusNLayoutHelper.ets  # One-to-N container layout component
|						|---- OnePlusNLayoutHelperEx.ets  # One-to-N extension container layout component
|						|---- RangeGridLayoutHelper.ets  # Region grid container layout component
|						|---- ScrollFixLayoutHelper.ets  # Scrolling-fix container layout component
|						|---- SingleLayoutHelper.ets  # Single-item container layout component
|						|---- StaggeredGridLayoutHelper.ets  # Water flow container layout component
|						|---- StickyLayoutHelper.ets  # Container layout component pinned to the top or to the bottom
|					|---- core  # Attribute folder
|						|---- VLayoutAttributes.ets  # Attributes of a container layout component
|     |---- README.md  # Readme    
|     |---- README_zh.md  # Readme                  
```

## How to Contribute

If you find any problem when using the project, submit an [issue](https://gitee.com/openharmony-sig/vlayout/issues) or a [PR](https://gitee.com/openharmony-sig/vlayout/pulls).

## License

This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-sig/vlayout/blob/master/LICENSE).
