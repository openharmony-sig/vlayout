/*
Copyright (c) 2021 Huawei Device Co., Ltd.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import {
  FIX_LAYOUT,
  SCROLL_FIX_LAYOUT,
  STICKY_LAYOUT,
  FLOAT_LAYOUT,
  LINEAR_LAYOUT,
  LinearAttributes,
  layoutDataType
} from '@ohos/vlayout'

enum AlignType {
  TOP_LEFT = 0,
  TOP_RIGHT = 1,
  BOTTOM_LEFT = 2,
  BOTTOM_RIGHT = 3
}

@Entry
@ComponentV2
struct FixSample {
  @Builder fixLayoutContent(item: layoutDataType, position: number | undefined) {
    Text(`${item}`)
      .width('100%')
      .height('100%')
      .backgroundColor(0x22EEEEEE)
      .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
      .fontSize(25)
      .fontColor('#999999')
      .fontWeight(FontWeight.Bold)
      .textAlign(TextAlign.Center)
      .textOverflow({ overflow: TextOverflow.Ellipsis })
      .onClick(() => {
        console.info('position = ' + position)
      })
  }

  @Builder scrollFixLayoutContent(item: layoutDataType, position: number | undefined) {
    Text(`${item}`)
      .width('100%')
      .height('100%')
      .backgroundColor(0x22EEEEEE)
      .border({ width: 0, color: '#000000', radius: 0, style: BorderStyle.Solid })
      .fontSize(18)
      .fontColor(0x000000)
      .fontWeight(FontWeight.Normal)
      .textAlign(TextAlign.Center)
      .textOverflow({ overflow: TextOverflow.Ellipsis })
      .onClick(() => {
        console.info('position = ' + position)
      })
  }

  @Builder stickyLayoutContent(item: layoutDataType, position: number | undefined) {
    Text(`${item}`)
      .width('100%')
      .height('100%')
      .backgroundColor(0x22EEEEEE)
      .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
      .fontSize(25)
      .fontColor('#999999')
      .fontWeight(FontWeight.Bold)
      .textAlign(TextAlign.Center)
      .textOverflow({ overflow: TextOverflow.Ellipsis })
      .onClick(() => {
        console.info('position = ' + position)
      })
  }

  @Builder floatLayoutContent(item: layoutDataType, position: number | undefined) {
    Text(`${item}`)
      .width('100%')
      .height('100%')
      .textAlign(TextAlign.Center)
      .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
  }

  @Builder linearLayoutContent(item: layoutDataType, position: number | undefined, listItemHeight: number | undefined) {
    Text(`${item}`)
      .width('100%')
      .height('100%')
      .backgroundColor('#33EEEEEE')
      .border({ width: 1, color: '#000000', radius: 0, style: BorderStyle.Solid })
      .fontColor('#999999')
      .fontSize(25)
      .fontWeight(FontWeight.Bold)
      .textAlign(TextAlign.Center)
      .textOverflow({ overflow: TextOverflow.Ellipsis })
      .onClick(() => {
        console.info('position = ' + position + ', item = ' + JSON.stringify(item))
      })
  }

  @Local bgColor: Color | string = Color.Pink
  @Local range: Array<number> = [0, 11]
  @Local stickyStart: boolean = true //控制STICKY_LAYOUT是否吸顶
  @Local linearLayoutAttribute: LinearAttributes = new LinearAttributes()

  aboutToAppear() {
    this.linearLayoutAttribute.range = this.range;
    this.linearLayoutAttribute.dividerHeight = 10;
    this.linearLayoutAttribute.aspectRatio = 1;
    this.linearLayoutAttribute.bgColor = this.bgColor;
    this.linearLayoutAttribute.padding = [5, 5, 5, 5];
    this.linearLayoutAttribute.margin = [5, 5, 5, 5];
  }

  build() {
    Stack() {
      List() {
        ListItem() {
          Button() {
            Text('transparency change')
          }
          .width('100%')
          .height(50)
          .backgroundColor(this.bgColor)
          .onClick(() => {
            this.linearLayoutAttribute.bgColor = 0x22EEEEEE
            this.linearLayoutAttribute.range = [0, 5]
          })
        }

        ListItem() {
          STICKY_LAYOUT({
            vLayoutContent: (item: layoutDataType , position: number | undefined) => {
              this.stickyLayoutContent(item, position)
            },
            vLayoutData: ['FixSample'],
            vLayoutAttribute: {
              aspectRatio: 4,
              bgColor: 0x33EEEEEE,
              stickyStart: this.stickyStart,
            }
          })
        }.sticky(this.stickyStart ? Sticky.Normal : Sticky.None)

        ListItem() {
          LINEAR_LAYOUT({
            vLayoutContent: (item: layoutDataType, position: number | undefined, listItemHeight: number | undefined) => {
              this.linearLayoutContent(item, position, listItemHeight)
            },
            vLayoutData: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            vLayoutAttribute: this.linearLayoutAttribute,
          })
        }
      }

      FIX_LAYOUT({
        vLayoutContent: (item: layoutDataType, position: number | undefined) => {
          this.fixLayoutContent(item, position)
        },
        vLayoutData: ['FIX(1)'],
        vLayoutAttribute: {
          layoutWidth: 100,
          layoutHeight: 100,
          //aspectRatio: 1,
          sketchMeasure: false,
          bgColor: this.bgColor,
          //xOffset: '2%',
          //yOffset: '1%',
          alignType: AlignType.TOP_LEFT,
          zIndex: 10,
        }
      })

      FIX_LAYOUT({
        vLayoutContent: (item: layoutDataType, position: number | undefined) => {
          this.fixLayoutContent(item, position)
        },
        vLayoutData: ['FIX(2)'],
        vLayoutAttribute: {
          layoutWidth: 100,
          layoutHeight: 100,
          //aspectRatio: 1,
          sketchMeasure: false,
          bgColor: this.bgColor,
          alignType: AlignType.TOP_RIGHT,
          zIndex: 10,
        }
      })

      SCROLL_FIX_LAYOUT({
        vLayoutContent: (item: layoutDataType, position: number | undefined) => {
          this.scrollFixLayoutContent(item, position)
        },
        vLayoutData: ['SCROLL'],
        vLayoutAttribute: {
          layoutWidth: 100,
          layoutHeight: 25,
          bgColor: this.bgColor,
          xOffset: '70%',
          yOffset: '30%',
        }
      })

      FLOAT_LAYOUT({
        vLayoutContent: (item: layoutDataType, position: number | undefined) => {
          this.floatLayoutContent(item, position)
        },
        vLayoutData: ['FLOAT'],
        vLayoutAttribute: {
          layoutWidth: 150,
          layoutHeight: 150,
          //aspectRatio: 1,
          bgColor: this.bgColor,
          //xOffset: '50%',
          //yOffset: 500,
          defaultLocation: [-50, -150],
          alignType: AlignType.BOTTOM_RIGHT,
          //padding: [10, 10, 10, 10],
          //margin: [10, 10, 10, 10],
        }
      })
    }
  }
}